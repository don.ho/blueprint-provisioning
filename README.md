# Blueprint for provisioning of cloud environments

A collection of scripts for provisioning a cloud environment. Uses:
* Hetzner cloud
* Packer
* Terraform
* Nomad
