# Set the variables in creds.auto.tfvars
variable "HCLOUD_TOKEN" {}
variable "TERRAFORM_SSH" {}

# Backend for storing the tfstate
terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "Budgetmeister"

    workspaces {
      name = "Zinsrechner"
    }
  }
  
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
    }
  }
}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = var.HCLOUD_TOKEN
}

resource "hcloud_ssh_key" "ho" {
  name = "Terraform SSH key Ho"
  public_key = file("ssh/id_rsa_ho.pub")
}

resource "hcloud_ssh_key" "terraform" {
  name = "Zinsrechner Terraform"
  public_key = file("ssh/id_rsa_terraform.pub")
}

resource "hcloud_ssh_key" "gitlab" {
  name = "Zinsrechner Gitlab"
  public_key = file("ssh/id_rsa_gitlab.pub")
}

data "hcloud_image" "packer_snapshot" {
  with_selector = "name=zire_basetemplate_ubuntu_20_04"
}

# Create a virtual machine for the web app
#  CX11: 1 vCPU, 2GB RAM, 20 GB SSD, 20 TB Traffic, 2.96 €/mo
#  nbg1: Nürnberg
resource "hcloud_server" "web" {
  name = "zire-vm"
  image = data.hcloud_image.packer_snapshot.id
  server_type = "cx11"
  location = "nbg1"
  ssh_keys = [hcloud_ssh_key.ho.id, hcloud_ssh_key.terraform.id, hcloud_ssh_key.gitlab.id]
}

resource "null_resource" "filecopy" {
  depends_on = [hcloud_server.web]
    
  provisioner "file" {
    source      = "scripts/deployDocker.sh"
    destination = "/root/deployDocker.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /root/deployDocker.sh"
    ]
  }
    
  connection {
      agent = false
      timeout = "1m"
      type = "ssh"
      user = "root"
      host = hcloud_server.web.ipv4_address
      private_key = var.TERRAFORM_SSH
  }
}

output "webapp_ip" {
  value = hcloud_server.web.ipv4_address
}