#!/bin/bash
if [ $# -eq 0 ]; then
    echo "Usage: deployDocker.sh <dockerImageTag>"
    exit 1
fi

containername=zire
registry=registry.gitlab.com/budgetmeister/kaufen-mieten-rechner

docker login -u $GITLAB_DEPLOY_TOKEN_USERNAME -p $GITLAB_DEPLOY_TOKEN_PASSWORD $registry

docker stop $containername
docker container rm $containername
docker run -d -p 80:80 --name $containername $registry:$1

docker logout $registry